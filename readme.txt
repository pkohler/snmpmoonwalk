Usage of snmpmwalk:
  -auth string
      SNMPv3 Authentication protocol [no, MD5, or SHA] (default "SHA")
  -bulk
      Toggles GetBulk for walking (will use GetNext otherwise)
  -c string
      Community string (SNMP v1 or v2) (default "public")
  -cmd string
      command to execute - [get, walk, or ping] (default "get")
  -n int
      number of requests (if pinging). Use 0 to query until stopped (eg. with ctrl+c) (default 10)
  -o string
      OID to get. Defaults to sysName. sysDescr (1.3.6.1.2.1.1.1.0) may also be convenient. (default "1.3.6.1.2.1.1.5.0")
  -outfile string
      where to send the output (default "stdout")
  -passa string
      authentication password (SNMPv3)
  -passp string
      privacy password (SNMPv3)
  -pause int
      time (in ms) to pause between GETs when PINGing (default 100)
  -priv string
      SNMPv3 Privacy protocol [no, AES, or DES] (default "DES")
  -s string
      server to query (IPv4/v6 or DNS.) (default "127.0.0.1")
  -t int
      timeout (in ms for get/ping, s for walk) (default 60)
  -user string
      username (SNMPv3)
  -v int
      SNMP version (1, 2, or 3) (default 2)
  -verbose
      enable verbose output for debugging
