package main

import (
	"flag"
	"fmt"
	prompt "github.com/Songmu/prompter"
	gosnmp "github.com/soniah/gosnmp"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
)

var (
	oid         string
	server      string
	repetitions int
	timeout     int
	version     int
	community   string
	cmd         string
	username    string
	passp       string
	passa       string
	verbose     bool
	authpro     string
	privpro     string
	authmatch   *regexp.Regexp
	privmatch   *regexp.Regexp
	logger      *log.Logger
	stdout      *log.Logger
	bulk        bool
	wait        int64
	outfile     string
)

func init() {
	// initialize command line arguments for parsing
	authmatch = regexp.MustCompile("(no|md5|sha)")
	privmatch = regexp.MustCompile("(no|aes|des)")
	const (
		oiduse = "OID to get. Defaults to sysName. sysDescr (1.3.6.1.2.1.1.1.0) may also be convenient."
		oiddef = "1.3.6.1.2.1.1.5.0"

		serveruse = "server to query (IPv4/v6 or DNS.)"
		serverdef = "127.0.0.1"

		repuse = "number of requests (if pinging). Use 0 to query until stopped (eg. with ctrl+c)"
		repdef = 10

		timeuse = "timeout (in ms for get/ping, s for walk)"
		timedef = 60

		veruse = "SNMP version (1, 2, or 3)"
		verdef = 2

		commuse = "Community string (SNMP v1 or v2)"
		commdef = "public"

		useruse = "username (SNMPv3)"
		userdef = ""

		passpuse = "privacy password (SNMPv3)"
		passpdef = ""

		passause = "authentication password (SNMPv3)"
		passadef = ""

		cmduse = "command to execute - [get, walk, or ping]"
		cmddef = "get"

		bulkuse = "Toggles GetBulk for walking (will use GetNext otherwise)"
		bulkdef = false

		authuse = "SNMPv3 Authentication protocol [no, MD5, or SHA]"
		authdef = "SHA"

		privuse = "SNMPv3 Privacy protocol [no, AES, or DES]"
		privdef = "DES"

		verbuse = "enable verbose output for debugging"
		verbdef = false

		waituse = "time (in ms) to pause between GETs when PINGing"
		waitdef = 100

		outfuse = "where to send the output"
		outfdef = "stdout"
	)
	flag.StringVar(&oid, "o", oiddef, oiduse)
	flag.StringVar(&server, "s", serverdef, serveruse)
	flag.StringVar(&cmd, "cmd", cmddef, cmduse)
	flag.IntVar(&repetitions, "n", repdef, repuse)
	flag.IntVar(&timeout, "t", timedef, timeuse)
	flag.IntVar(&version, "v", verdef, veruse)
	flag.BoolVar(&bulk, "bulk", bulkdef, bulkuse)
	flag.StringVar(&community, "c", commdef, commuse)
	flag.StringVar(&username, "user", userdef, useruse)
	flag.StringVar(&passp, "passp", passpdef, passpuse)
	flag.StringVar(&passa, "passa", passadef, passause)
	flag.BoolVar(&verbose, "verbose", verbdef, verbuse)
	flag.StringVar(&authpro, "auth", authdef, authuse)
	flag.StringVar(&privpro, "priv", privdef, privuse)
	flag.Int64Var(&wait, "pause", waitdef, waituse)
	flag.StringVar(&outfile, "outfile", outfdef, outfuse)
}

func logInfo(text string) {
	stdout.Println(text)
	if logger != nil {
		logger.Println(text)
	}
}

func logFatal(text string) {
	stdout.SetFlags(log.Lshortfile)
	stdout.Println(text)
	if logger != nil {
		logger.SetFlags(log.LstdFlags | log.Lshortfile)
		logger.Fatal(text)
	} else {
		os.Exit(1)
	}
}

func configureV3Sec(snmp *gosnmp.GoSNMP) {
	// change protocol args to lowercase for easier validation
	privpro = strings.ToLower(privpro)
	authpro = strings.ToLower(authpro)
	// initialize the security
	snmp.SecurityModel = gosnmp.UserSecurityModel
	sec := &gosnmp.UsmSecurityParameters{}

	// configure security; start by making sure we've got a username
	if username == "" {
		username = prompt.Prompt("Username:", "")
	}
	// configure authentication protocol
	if !authmatch.MatchString(authpro) {
		logFatal("invalid authentication protocol - it must be one of 'no', 'MD5', or 'SHA'\n")
	} else {
		switch authpro {
		case "no":
			sec.AuthenticationProtocol = gosnmp.NoAuth
		case "md5":
			sec.AuthenticationProtocol = gosnmp.MD5
		case "sha":
			sec.AuthenticationProtocol = gosnmp.SHA
		}
	}
	// make sure we've got a valid auth password
	if sec.AuthenticationProtocol != gosnmp.NoAuth && passa == "" {
		passa = prompt.Password("Authentication Password:")
	}
	// configure privacy protocol
	if !privmatch.MatchString(privpro) {
		logFatal("invalid privacy protocol - it must be one of 'no', 'AES', or 'DES'\n")
	} else {
		switch privpro {
		case "no":
			sec.PrivacyProtocol = gosnmp.NoPriv
		case "aes":
			sec.PrivacyProtocol = gosnmp.AES
		case "des":
			sec.PrivacyProtocol = gosnmp.DES
		}
	}
	// make sure we've got a valid privacy password
	if sec.PrivacyProtocol != gosnmp.NoPriv && passp == "" {
		passp = prompt.Password("Privacy Password:")
	}
	logInfo(fmt.Sprintf("using SNMPv3 with %s privacy and %s security", privpro, authpro))
	// load those values into the security struct
	sec.UserName = username
	sec.AuthenticationPassphrase = passa
	sec.PrivacyPassphrase = passp
	// choose the right security model based on what's been entered.
	if sec.AuthenticationProtocol == gosnmp.NoAuth && sec.PrivacyProtocol == gosnmp.NoPriv {
		snmp.MsgFlags = gosnmp.NoAuthNoPriv
	} else if sec.AuthenticationProtocol != gosnmp.NoAuth && sec.PrivacyProtocol == gosnmp.NoPriv {
		snmp.MsgFlags = gosnmp.AuthNoPriv
	} else {
		snmp.MsgFlags = gosnmp.AuthPriv
	}
	snmp.SecurityParameters = sec
}

func buildSnmpDialer() *gosnmp.GoSNMP {
	// initialize the snmp connector
	snmp := &gosnmp.GoSNMP{
		Target:  server,
		Port:    161,
		Timeout: time.Duration(timeout) * time.Millisecond,
	}
	// configure logging
	if verbose {
		if logger != nil {
			snmp.Logger = logger
		} else {
			snmp.Logger = log.New(os.Stdout, "DEBUG ", 0)
		}
	}
	// configure it for the correct version of the SNMP protocol
	var snmpver gosnmp.SnmpVersion
	switch version {
	case 1:
		snmpver = gosnmp.Version1
	case 2:
		snmpver = gosnmp.Version2c
	case 3:
		snmpver = gosnmp.Version3
	default:
		logInfo("invalid -v argument:")
		flag.PrintDefaults()
		os.Exit(1)
	}
	snmp.Version = snmpver
	// figure out security settings
	if version == 3 {
		configureV3Sec(snmp)
	} else {
		// SNMP v1 and v2 just use a straightforward community string
		logInfo(fmt.Sprintf("using SNMPv%d with community string: %s", version, community))
		snmp.Community = community
	}
	return snmp
}

func pduToStr(value gosnmp.SnmpPDU) string {
	var resp string
	switch value.Type {
	case gosnmp.Boolean:
		bval := (value.Value == 1)
		resp = fmt.Sprintf("%s: Bool: %v", value.Name, bval)
	case gosnmp.Integer:
		resp = fmt.Sprintf("%s: Int: %d", value.Name, value.Value)
	case gosnmp.OctetString:
		resp = fmt.Sprintf("%s: OctStr: \"%s\"", value.Name, string(value.Value.([]byte)))
	case gosnmp.ObjectIdentifier:
		resp = fmt.Sprintf("%s: OID: %s", value.Name, value.Value)
	case gosnmp.ObjectDescription:
		resp = fmt.Sprintf("%s: OIDDescr: %s", value.Name, string(value.Value.([]byte)))
	case gosnmp.IPAddress:
		resp = fmt.Sprintf("%s: IPAddr: %s", value.Name, value.Value)
	case gosnmp.Counter32:
		resp = fmt.Sprintf("%s: Count32: %d", value.Name, gosnmp.ToBigInt(value.Value))
	case gosnmp.Gauge32:
		resp = fmt.Sprintf("%s: Gauge32: %d", value.Name, gosnmp.ToBigInt(value.Value))
	case gosnmp.TimeTicks:
		resp = fmt.Sprintf("%s: TimeTicks: %d", value.Name, gosnmp.ToBigInt(value.Value))
	case gosnmp.Opaque:
		resp = fmt.Sprintf("%s: Opaque: %s", value.Name, string(value.Value.([]byte)))
	case gosnmp.NsapAddress:
		resp = fmt.Sprintf("%s: NsapAddr: %s", value.Name, string(value.Value.([]byte)))
	case gosnmp.Counter64:
		resp = fmt.Sprintf("%s: Count64: %d", value.Name, gosnmp.ToBigInt(value.Value))
	case gosnmp.Uinteger32:
		resp = fmt.Sprintf("%s: Uint32: %d", value.Name, gosnmp.ToBigInt(value.Value))
	default:
		resp = fmt.Sprintf("%s: TYPE %d: %d", value.Name, value.Type, gosnmp.ToBigInt(value.Value))
	}
	return resp
}

func logVal(value gosnmp.SnmpPDU) error {
	logInfo(pduToStr(value))
	return nil //need to return an error for the walk callback
}

func ping(snmp *gosnmp.GoSNMP) {
	logInfo(fmt.Sprintf(
		"SNMP \"ping\" started - pausing %dms between requests, and waiting up to %dms for responses",
		wait,
		timeout,
	))
	logInfo(fmt.Sprintf("beginning %d GET requests for OID %s from %s...\n", repetitions, oid, server))
	// get ready to ping
	oids := []string{oid}
	var start time.Time
	var resptime, totalresptime time.Duration
	var successes, failures int64
	successes = 0
	failures = 0
	totalresptime = 0
	for i := 0; i < repetitions; i++ {
		start = time.Now()
		result, err := snmp.Get(oids)
		resptime = time.Now().Sub(start)
		if err != nil {
			logInfo(fmt.Sprintf("SNMP get error: %v", err))
			failures++
		} else {
			var resp string
			for _, variable := range result.Variables {
				resp = pduToStr(variable)
				logInfo(fmt.Sprintf("Reply from %s: time = %v; %s", server, resptime, resp))
			}
			successes++
			totalresptime += resptime
		}
		time.Sleep(time.Duration(wait) * time.Millisecond)
	}
	logInfo(fmt.Sprintf(
		"\nSucceeded: %d, Failed %d, Success Rate: %0.2f%%",
		successes,
		failures,
		float64(successes)/float64(repetitions)*100,
	))
	sillyvar := time.Duration(successes)
	logInfo(fmt.Sprintf("Average response time: %v\n", totalresptime/sillyvar))
}

func get(snmp *gosnmp.GoSNMP) {
	oids := []string{oid}
	result, err := snmp.Get(oids)
	if err != nil {
		logFatal(fmt.Sprintf("%v", err))
	} else {
		for _, variable := range result.Variables {
			logInfo(fmt.Sprintf("%s - %s", server, pduToStr(variable)))
		}
	}
}

func walk(snmp *gosnmp.GoSNMP) {
	logInfo(fmt.Sprintf("walking %s beginning at %s...", server, oid))
	start := time.Now()
	if bulk {
		logInfo("using GetBulk requests of up to 50 OIDs at a time")
		snmp.BulkWalk(oid, logVal)
	} else {
		logInfo("using GetNext to walk...")
		snmp.Walk(oid, logVal)
	}
	logInfo(fmt.Sprintf("walk completed in %v", time.Now().Sub(start)))
}

func main() {
	flag.Parse()
	stdout = log.New(os.Stdout, "", 0)
	if outfile != "stdout" {
		logfile, err := os.Create(outfile)
		if err != nil {
			fmt.Printf("error creating/opening %s:\n%v", outfile, err)
			os.Exit(4)
		} else {
			logger = log.New(logfile, "", log.LstdFlags)
		}
	}
	logInfo("building SNMP dialer...")
	snmp := buildSnmpDialer()
	if strings.ToLower(cmd) == "walk" {
		// increase timeout from ms to s
		snmp.Timeout *= 1000
	}
	// initialize the connection
	err := snmp.Connect()
	if err != nil {
		logFatal(fmt.Sprintf("SNMP connection error: %v", err))
	}
	defer snmp.Conn.Close()
	switch strings.ToLower(cmd) {
	case "ping":
		ping(snmp)
	case "get":
		get(snmp)
	case "walk":
		walk(snmp)
	default:
		logInfo("invalid -cmd argument:")
		flag.PrintDefaults()
	}
}
